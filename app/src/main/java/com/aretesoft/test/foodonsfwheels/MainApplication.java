package com.aretesoft.test.foodonsfwheels;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by JoseM on 29/11/16.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
