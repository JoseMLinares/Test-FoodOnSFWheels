package com.aretesoft.test.foodonsfwheels.foodtrucks;

import java.util.List;

/**
 * Created by JoseM on 29/11/16.
 */
public class FoodTrucksData {

    private static List<FoodTruck> foodTruckList;
    private static FoodTrucksData ourInstance = new FoodTrucksData();

    public static FoodTrucksData getInstance() {
        return ourInstance;
    }

    private FoodTrucksData() {
    }

    public static List<FoodTruck> getFoodTruckList() {
        return foodTruckList;
    }

    public static void setFoodTruckList(List<FoodTruck> foodTruckList) {
        FoodTrucksData.foodTruckList = foodTruckList;
    }
}
