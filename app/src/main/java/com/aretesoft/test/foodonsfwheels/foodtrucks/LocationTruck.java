package com.aretesoft.test.foodonsfwheels.foodtrucks;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by JoseM on 29/11/16.
 */

public class LocationTruck{
    @SerializedName("type")
    String type;

    @SerializedName("coordinates")
    ArrayList<Double> coordinates = new ArrayList<>();

    public LocationTruck() {
    }

    public LocationTruck(String type, ArrayList<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }
}
