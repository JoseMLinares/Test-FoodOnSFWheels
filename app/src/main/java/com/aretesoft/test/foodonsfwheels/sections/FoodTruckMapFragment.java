package com.aretesoft.test.foodonsfwheels.sections;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.aretesoft.test.foodonsfwheels.R;
import com.aretesoft.test.foodonsfwheels.foodtrucks.FoodTruck;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.List;

import io.realm.Realm;

/**
 * Created by JoseM on 29/11/16.
 */

public class FoodTruckMapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "FoodOnSFWheels";

    private static final int PETICION_PERMISO_LOCALIZACION = 101;

    private GoogleMap map;
    private GoogleApiClient apiClient;

    Realm realm;

    private List<FoodTruck> foodTruckList;
    private Location userLocation = new Location("userLocation");

    public FoodTruckMapFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm=Realm.getDefaultInstance();
        apiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        foodTruckList=realm.where(FoodTruck.class).findAll();
        LatLng locationTruck;
        Double lat = .0;
        Double lng = .0;
        String nameTruck;

        for(int i=0; i<foodTruckList.size();i++){
            lat=Double.parseDouble(foodTruckList.get(i).getLatitude());
            lng=Double.parseDouble(foodTruckList.get(i).getLongitude());
            nameTruck=foodTruckList.get(i).getApplicant();
            locationTruck= new LatLng(lat, lng);

            //Metodo desactivado por ser menos preciso con distancias menores de 20km
            //if(distanceToFoodTruck(lat, lng))

            //Metodo mas preciso, haciendo uso de Android Location.
            if(googleDistanceToFoodTruck(locationTruck))
                map.addMarker((new MarkerOptions().position(locationTruck).title(nameTruck)));
        }

        //TODO Borrar Fake test
        float zoom = 10;
        locationTruck= new LatLng(37.747575, -122.444315);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(locationTruck, zoom));
        //TODO Fin Fake test





    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //Conectado correctamente a Google Play Services

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PETICION_PERMISO_LOCALIZACION);
        } else {

            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);
            //TODO Habilitar despues de borrar fake test
            //setUserLocation(lastLocation);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Ha ocurrido un error un google services.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Ha ocurrido un error un google services.");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PETICION_PERMISO_LOCALIZACION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                @SuppressWarnings("MissingPermission")
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);
                //TODO Habilitar despues de borrar fake test
                //setUserLocation(lastLocation);


            } else {
                Log.e(TAG, "Permiso denegado");
            }
        }
    }

    ///////////////////
    ///// Methods /////
    ///////////////////

    public Location getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(Location userLocation) {
        this.userLocation = userLocation;
    }

    /**
     * Calcula la distancia entre la geoposicion del usuario y el foodtruck, haciendo uso
     * de la propia herramienta de Android Location. Mucho mas precisa para distancias pequeñas.
     * @param foodTruckLatLng
     * @return
     */
    //TODO Esto hay que cambiarlo, queda fatal, hacer un check permisos antes de userlocation
    @SuppressWarnings("MissingPermission")
    private boolean googleDistanceToFoodTruck(LatLng foodTruckLatLng){
        LatLng userLatLng;

        double distance;
        //SphericalUtil.computeDistanceBetween()

        if(userLocation!=null) {
            //userLatLng = new LatLng(getUserLocation().getLatitude(),getUserLocation().getLongitude());
            //TODO BORRAR FAKE TEST
            userLatLng = new LatLng(37.747575, -122.444315);
            distance = SphericalUtil.computeDistanceBetween(foodTruckLatLng, userLatLng);
        }
        else{
            //TODO QUITAR fake test y habilitar el codigo de localizacion

            //TODO BORRAR FAKE TEST
            userLatLng = new LatLng(37.747575, -122.444315);
            //SI estamos en españa no va mostrar nunca nada
            //Location userLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);
            //setUserLocation(userLocation);
            //userLatLng = new LatLng(getUserLocation().getLatitude(),getUserLocation().getLongitude());
            distance = SphericalUtil.computeDistanceBetween(foodTruckLatLng, userLatLng);
        }

        //TODO El usuario deberia poder configurar desde userSettings los parametros de distancia.
        if(distance<=3000){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Calcula la distancia entre la geoposicion del usuario y el foodTruck, haciendo uso de la
     * formula de Haversine.
     * @param lat
     * @param lng
     * @return boolean
     */
    private boolean distanceToFoodTruck(Double lat, Double lng){
        //Calculo de distancia entre dos puntos geograficos
        Double latUser=.0, lngUser=.0;

        //Posicion usuario
        //latUser=getUserLocation().getLatitude();
        //lngUser=getUserLocation().getLongitude();

        //TODO Borrar fake test y habilitar codigo correcto
        latUser=37.747575;
        lngUser=-122.444315;

        //Diferencia entre Punto A y Punto B
        Double Alat=lat-latUser, Alng=lng-lngUser;

        //Distancia entre dos puntos geograficos=> d=r.c , donde r es el radio de la tierra (aprox)
        Double a = Math.pow(Math.sin(Alat/2),2)+Math.cos(latUser) * Math.cos(lat) * Math.pow(Math.sin(Alng/2),2);

        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        Double r = 6378.0;
        Double d = r * c;
        Log.e(TAG, "Distancia: "+d);

        //Vamos a usar 3km por defecto, enrealidad deberiamos darle al usuario la opcion de cambiarlo.
        if(d<=3){
            return true;
        }else
            return false;

    }

}
