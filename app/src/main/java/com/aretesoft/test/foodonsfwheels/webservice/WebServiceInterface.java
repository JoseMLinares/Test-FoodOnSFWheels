package com.aretesoft.test.foodonsfwheels.webservice;

import com.aretesoft.test.foodonsfwheels.foodtrucks.FoodTruck;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by JoseM on 29/11/16.
 */

public interface WebServiceInterface {

    @GET("6a9r-agq8.json")
    Call<List<FoodTruck>> getFoodTrucksData();
}

