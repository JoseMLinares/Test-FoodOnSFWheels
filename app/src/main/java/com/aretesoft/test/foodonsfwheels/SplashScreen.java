package com.aretesoft.test.foodonsfwheels;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.aretesoft.test.foodonsfwheels.foodtrucks.FoodTruck;
import com.aretesoft.test.foodonsfwheels.foodtrucks.FoodTrucksData;
import com.aretesoft.test.foodonsfwheels.webservice.WebServiceClient;
import com.aretesoft.test.foodonsfwheels.webservice.WebServiceInterface;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JoseM on 29/11/16.
 */

public class SplashScreen extends AppCompatActivity {

    List<FoodTruck> foodTruckList;

    ProgressBar progressBar;

    int maxTime = 3000;
    Calendar initialTime, finalTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        initialTime = Calendar.getInstance();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        WebServiceInterface servicesInterface = WebServiceClient.getClient().create(WebServiceInterface.class);

        Call<List<FoodTruck>> callJsonFoodTrucks = servicesInterface.getFoodTrucksData();

        callJsonFoodTrucks.enqueue(new Callback<List<FoodTruck>>() {
            @Override
            public void onResponse(Call<List<FoodTruck>> call, Response<List<FoodTruck>> response) {
                finalTime=Calendar.getInstance();
                foodTruckList=response.body();

                FoodTrucksData.setFoodTruckList(foodTruckList);


                final long elapsedTime=finalTime.getTimeInMillis()-initialTime.getTimeInMillis();

                if(elapsedTime < maxTime){
                    Thread timerTread = new Thread(){
                        public void run(){
                            try{
                                sleep(maxTime-elapsedTime);
                            }
                            catch (InterruptedException e){
                                e.printStackTrace();
                            } finally {
                                Intent intent = new Intent(SplashScreen.this, Main.class);
                                startActivity(intent);
                            }
                        }
                    };
                    timerTread.start();
                }else{
                    Intent intent = new Intent(SplashScreen.this, Main.class);
                    startActivity(intent);
                }

                progressBar.setVisibility(View.INVISIBLE);


            }

            @Override
            public void onFailure(Call<List<FoodTruck>> call, Throwable t) {
                Log.e("ERROR", "Error: "+t);
            }
        });


    }
}
