package com.aretesoft.test.foodonsfwheels.foodtrucks;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.realm.RealmObject;

/**
 * Created by JoseM on 29/11/16.
 */

public class FoodTruck extends RealmObject{

    //@SerializedName("address")
    String address;

    //@SerializedName("applicant")
    String applicant;

    //@SerializedName("block")
    String block;

    //@SerializedName("blocklot")
    String blocklot;

    //@SerializedName("cnn")
    String cnn;

    //@SerializedName("dayshours")
    String dayshours;

    //@SerializedName("expirationdate")
    String expirationdate;

    //@SerializedName("facilitytype")
    String facilitytype;

    //@SerializedName("fooditems")
    String fooditems;

    //@SerializedName("latitude")
    String latitude;

    //@SerializedName("location")
    //LocationTruck locationTruck;

    //@SerializedName("locationdescription")
    String locationdescription;

    //@SerializedName("longitude")
    String longitude;

    //@SerializedName("lot")
    String lot;

    //@SerializedName("objectid")
    String objectid;

    //@SerializedName("permit")
    String permit;

    //@SerializedName("priorpermit")
    String priorpermit;

    //@SerializedName("received")
    String received;

    //@SerializedName("schedule")
    String schedule;

    //@SerializedName("status")
    String status;

    //@SerializedName("x")
    String x;

    //@SerializedName("y")
    String y;

    public FoodTruck() {
    }

    public FoodTruck(String address, String applicant, String block, String blocklot, String cnn, String dayshours, String expirationdate, String facilitytype, String fooditems, String latitude, LocationTruck locationTruck, String locationdescription, String longitude, String lot, String objectid, String permit, String priorpermit, String received, String schedule, String status, String x, String y) {
        this.address = address;
        this.applicant = applicant;
        this.block = block;
        this.blocklot = blocklot;
        this.cnn = cnn;
        this.dayshours = dayshours;
        this.expirationdate = expirationdate;
        this.facilitytype = facilitytype;
        this.fooditems = fooditems;
        this.latitude = latitude;
        //this.locationTruck = locationTruck;
        this.locationdescription = locationdescription;
        this.longitude = longitude;
        this.lot = lot;
        this.objectid = objectid;
        this.permit = permit;
        this.priorpermit = priorpermit;
        this.received = received;
        this.schedule = schedule;
        this.status = status;
        this.x = x;
        this.y = y;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getBlocklot() {
        return blocklot;
    }

    public void setBlocklot(String blocklot) {
        this.blocklot = blocklot;
    }

    public String getCnn() {
        return cnn;
    }

    public void setCnn(String cnn) {
        this.cnn = cnn;
    }

    public String getDayshours() {
        return dayshours;
    }

    public void setDayshours(String dayshours) {
        this.dayshours = dayshours;
    }

    public String getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        this.expirationdate = expirationdate;
    }

    public String getFacilitytype() {
        return facilitytype;
    }

    public void setFacilitytype(String facilitytype) {
        this.facilitytype = facilitytype;
    }

    public String getFooditems() {
        return fooditems;
    }

    public void setFooditems(String fooditems) {
        this.fooditems = fooditems;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /*public LocationTruck getLocationTruck() {
        return locationTruck;
    }

    public void setLocationTruck(LocationTruck locationTruck) {
        this.locationTruck = locationTruck;
    }*/

    public String getLocationdescription() {
        return locationdescription;
    }

    public void setLocationdescription(String locationdescription) {
        this.locationdescription = locationdescription;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getObjectid() {
        return objectid;
    }

    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getPriorpermit() {
        return priorpermit;
    }

    public void setPriorpermit(String priorpermit) {
        this.priorpermit = priorpermit;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }



}
